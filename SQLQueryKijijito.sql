USE [master]
GO
/****** Object:  Database [kijijito_identity]    Script Date: 2020-06-08 7:13:13 PM ******/
CREATE DATABASE [kijijito_identity]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'kijijito_identity', FILENAME = N'/var/opt/mssql/data/kijijito_identity.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'kijijito_identity_log', FILENAME = N'/var/opt/mssql/data/kijijito_identity_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [kijijito_identity] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [kijijito_identity].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [kijijito_identity] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [kijijito_identity] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [kijijito_identity] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [kijijito_identity] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [kijijito_identity] SET ARITHABORT OFF 
GO
ALTER DATABASE [kijijito_identity] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [kijijito_identity] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [kijijito_identity] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [kijijito_identity] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [kijijito_identity] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [kijijito_identity] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [kijijito_identity] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [kijijito_identity] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [kijijito_identity] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [kijijito_identity] SET  ENABLE_BROKER 
GO
ALTER DATABASE [kijijito_identity] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [kijijito_identity] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [kijijito_identity] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [kijijito_identity] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [kijijito_identity] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [kijijito_identity] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [kijijito_identity] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [kijijito_identity] SET RECOVERY FULL 
GO
ALTER DATABASE [kijijito_identity] SET  MULTI_USER 
GO
ALTER DATABASE [kijijito_identity] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [kijijito_identity] SET DB_CHAINING OFF 
GO
ALTER DATABASE [kijijito_identity] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [kijijito_identity] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [kijijito_identity] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'kijijito_identity', N'ON'
GO
ALTER DATABASE [kijijito_identity] SET QUERY_STORE = OFF
GO
USE [kijijito_identity]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[Province] [nvarchar](max) NULL,
	[PostalCode] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
	[RegistrationTS] [datetime2](7) NOT NULL,
	[Rating] [real] NOT NULL,
	[Secret] [nvarchar](max) NULL,
	[SecretCreationTS] [datetime2](7) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [int] NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Conversations]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conversations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SenderId] [int] NOT NULL,
	[ReceiverId] [int] NOT NULL,
	[Message] [nvarchar](1000) NOT NULL,
	[Status] [int] NOT NULL,
	[CreationTS] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Image1] [nvarchar](max) NULL,
	[Image2] [nvarchar](max) NULL,
	[Image3] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[PostedTS] [datetime2](7) NOT NULL,
	[Category] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 2020-06-08 7:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RatedUserId] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreationTS] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200602234141_CreateIdentitySchema', N'3.1.4')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Facebook', N'10157647848658458', N'Facebook', 1017)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'101104527298971785019', N'Google', 1017)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'103234575265278778569', N'Google', 1016)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'113644407270356878940', N'Google', 2015)
GO
SET IDENTITY_INSERT [dbo].[AspNetUsers] ON 
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (1, N'Pepe', N'PEPE', N'TestUser@mail.com', N'TESTUSER@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEJHjmQO2ZWw5jN4WSAKJRbNpjXyxzTmVAviX8rc22qv+fO8iB3jjiZ1ZCy5i39TfoA==', N'RPFWVGL4APBM774PN6NUDRSPYCPNJLN6', N'7e740de1-fc12-4b65-b6bf-9c07462538ec', NULL, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (7, N'Bruce Wayne', N'BRUCE WAYNE', N'bruce@mail.com', N'BRUCE@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEFj+qvHu+FKzFk1chsIp1GZKTIBS6v2vLMFMR46J2hG363gV5BnFhiXZi4rIplViaQ==', N'K3R6DPFQ5LIGGDOCWQNPQLCQNQZW732S', N'77156bb4-d26a-49de-adb2-818b911b6ebc', N'418 418 4184', 0, 0, NULL, 1, 0, N'Main 123', N'Montreal', N'QC', N'H3E 1X2', N'Bruce_Wayne.jpg', CAST(N'2020-06-03T12:15:01.4133333' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (8, N'Bob', N'BOB', N'bob@mail.com', N'BOB@MAIL.COM', 0, N'AQAAAAEAACcQAAAAEFYqmNlkKhdXCWEmAsHwScHMN6jfmWL27JG/OMdw0ERD0NIqNOcoGUuUPL2fxUx7wg==', N'WCAWZ2C56SWLIUSYLD6ZPO4AYBUR2LBC', N'e8cbe599-28e3-4832-a16e-ff2fdc65f08d', N'123 456 7890', 0, 0, NULL, 1, 0, N'Pineapple 999', N'Montreal', N'QC', N'H2E 2X2', N'Bob.jpg', CAST(N'2020-06-03T12:49:58.3100000' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (9, N'JerryLee', N'JERRYLEE', N'jerry@mail.com', N'JERRY@MAIL.COM', 1, N'AQAAAAEAACcQAAAAECyu299MIx5YYuqOSb+fMVQAptEMRgWL+omORVRvd41yzs1C2MdKbFtSv8IXjQzexw==', N'YT6X73X3CO7HBZ2UTYCB3POXCLCGRG6W', N'4d0185d4-accd-4f94-943e-235609a5fb33', N'2514445555', 0, 0, NULL, 1, 0, N'123 My Street', N'Montreal', N'QC', N'H4H5H6', N'JerryLee.png', CAST(N'2020-06-03T13:22:27.6100000' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (10, N'Diana', N'DIANA', N'diana@mail.com', N'DIANA@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEIXIVZU2zZ91HOpvNe2GUgUbF9tVpyCHvd10QpE2eoBMPo2uIDn5s0+DnDCq3iL0HQ==', N'6TJGATYG33PIRD3JEQZKWQ6ACVLOEITR', N'094db2c3-8c70-40d9-8735-27aa4e987f99', N'418 985 6523', 0, 0, NULL, 1, 0, N'Justice Hall 654', N'Montreal', N'QC', N'H3E 1X2', N'Diana.jpg', CAST(N'2020-06-04T18:36:23.2866667' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (11, N'Pablo', N'PABLO', N'pablo@mail.com', N'PABLO@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEHDM+hJDckCgC+HksBYCFvQHRFzvd3+T7IU1aJ0wUQdt03kamPU57fefA7HPS9IACw==', N'OXRIOADOO3GYF7ARFKIPKJ4D5SB7BATJ', N'b27119d6-f64d-4e27-9e5b-e0cddef2ce22', N'419 987 6542', 0, 0, NULL, 1, 0, N'Jolicoeur 198', N'Montreal', N'QC', N'H3E 1X3', N'Pablo.jpg', CAST(N'2020-06-04T19:30:14.2600000' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (12, N'Richard', N'RICHARD', N'richard@mail.com', N'RICHARD@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEE1/gjTXTPIq8L2OMnM6vpovIUR3biq9vUQ9tWy1vMI97cU/b1GX27hFUMJwp45Djg==', N'OQBQ5LROU7RIHNMYQUHFSTYYRT6VFZ2T', N'9815f1d1-2e7d-41df-92f4-2c774b861797', N'514 532 9741', 0, 0, NULL, 1, 0, N'567 Main Av.', N'Montreal', N'QC', N'H3E 1X2', N'Richard.jpg', CAST(N'2020-06-04T19:38:47.5066667' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (13, N'Rossana', N'ROSSANA', N'rossana@mail.com', N'ROSSANA@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEBx3EXN2J+LmIdPrkL80yRecUgFOqhsHOPoTMk/3JAJ0cg5AlghVPO9tRZQtLk5dtA==', N'DL7UJLU3RCGDXH3PZJWYWY6DSGMU3N7N', N'38a41f35-2d49-4746-9f7e-9e7765344a19', N'514 985 3214', 0, 0, NULL, 1, 0, N'61 Mazarin', N'Montreal', N'QC', N'H4E 2X1', N'Rossana.jpg', CAST(N'2020-06-04T19:54:07.7833333' AS DateTime2), 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (14, N'Andrea', N'ANDREA', N'andrea@mail.com', N'ANDREA@MAIL.COM', 1, N'AQAAAAEAACcQAAAAECdbBxNsFIl9rqcgds+1H/7Rb1W4kVjPWBRxIHNh9YEvD8SAmtYdMsBf6/aA866Hkw==', N'35WJBKRW76KKN2XA4AUA5TPK3ZSZUECW', N'd4e51bfa-28b3-406b-b38f-b62a6518b372', N'514 789 1058', 0, 0, NULL, 1, 0, N'Av. Busch 321', N'Montreal', N'QC', N'H4E 2X1', NULL, CAST(N'2020-06-04T20:42:06.7533333' AS DateTime2), 0, NULL, CAST(N'2020-06-04T20:42:06.7533333' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (15, N'Alfred', N'ALFRED', N'alfred@mail.com', N'ALFRED@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEPLd65l5gfQaoyVu8bWZs2nau4m7oxEXTkSsGMtNlSDKw7SWPa2D6SKjbuFDp7o7rQ==', N'OLOW2I6MJP6WDPAUZJDXMCUK3NIGT3WV', N'942c6e2d-b74f-4b49-b794-e470c1b987ae', N'514 895 3214', 0, 0, NULL, 1, 0, N'84 Main St', N'Montreal', N'QC', N'H4E 1X1', NULL, CAST(N'2020-06-04T21:16:34.1566667' AS DateTime2), 0, NULL, CAST(N'2020-06-04T21:16:34.1566667' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (16, N'Barbara Gordon', N'BARBARA GORDON', N'barbara@mail.com', N'BARBARA@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEOXJYCgGML99zAooNLi9zjxVqGeQVm4e/8SGShQZ/RH+gVODe9yEAOjP5PUgcxsWtQ==', N'5NNSHQH5L4GHRCAEMTYF4PDK2R3OS3J6', N'ac353c15-7d45-4f7a-8c10-899ca5a7135f', N'514 963 7895', 0, 0, NULL, 1, 0, N'Second St 123', N'Montreal', N'QC', N'H3E 1X2', NULL, CAST(N'2020-06-04T21:18:57.6300000' AS DateTime2), 0, NULL, CAST(N'2020-06-04T21:18:57.6300000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (1015, N'TomMtl', N'TOMMTL', N'tom@mail.com', N'TOM@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEMvsxZtb66Y5VcWWzffODwPkPKYC07LW2xyREALyfDOAFx//FKH+/bWytgXwv+YEOA==', N'LUFIKHVY3D22KVSKCIZZX7NBZLRI4VPN', N'9db9d690-469c-4fa8-81bc-3fdc93857513', N'555666444', 0, 0, NULL, 1, 0, N'23 Main Street', N'Montreal', N'QC', N'H7T8T9', N'TomMtl.png', CAST(N'2020-06-05T17:28:50.6500000' AS DateTime2), 0, NULL, CAST(N'2020-06-05T17:28:50.6500000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (1016, N'scat.bolivia@gmail.com', N'SCAT.BOLIVIA@GMAIL.COM', N'scat.bolivia@gmail.com', N'SCAT.BOLIVIA@GMAIL.COM', 1, NULL, N'HSS2SK3X3VAEBB4TL7JPJAGURGUOQ6YC', N'8a441fe7-1335-488f-932f-6a27e9c077f2', NULL, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-06-05T17:30:47.3766667' AS DateTime2), 0, NULL, CAST(N'2020-06-05T17:30:47.3766667' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (1017, N'Sau Lo', N'SAU LO', N'saulo.carranza@gmail.com', N'SAULO.CARRANZA@GMAIL.COM', 0, NULL, N'E2WHZWG5R4T3DCDVVCSMXEQZRTPKYGNV', N'ebbfceaa-3dd8-4af2-8c42-3d0a16aacac3', NULL, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-06-05T18:42:34.8466667' AS DateTime2), 0, NULL, CAST(N'2020-06-05T18:42:34.8466667' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (2014, N'fdasfd', N'FDASFD', N'fdsa@fdsfd.com', N'FDSA@FDSFD.COM', 1, N'AQAAAAEAACcQAAAAEFdIoP8HzYZycW3SxpFbSXfsGbtaWOS0tUdUwKs9k4Jb+iswfT8ovkjDyasm8jKVFQ==', N'4VWBC7VK37NVXYIF3IIJLM53LOVUAAZT', N'e60117ff-e620-4e44-b8b5-3239ff8f3077', N'654654', 0, 0, NULL, 1, 0, N'fdsfds', N'MOntreal', N'QC', N'H4H5H6', NULL, CAST(N'2020-06-08T08:52:38.8100000' AS DateTime2), 0, NULL, CAST(N'2020-06-08T08:52:38.8100000' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (2015, N'giovanaourique@gmail.com', N'GIOVANAOURIQUE@GMAIL.COM', N'giovanaourique@gmail.com', N'GIOVANAOURIQUE@GMAIL.COM', 0, NULL, N'FNET3JESDU3ZXPXJQ6QUVQ5EO35BUYY4', N'2327ec7d-bf1f-41ca-ba98-f68ef7e0778a', NULL, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-06-08T09:00:36.5666667' AS DateTime2), 0, NULL, CAST(N'2020-06-08T09:00:36.5666667' AS DateTime2))
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Address], [City], [Province], [PostalCode], [Picture], [RegistrationTS], [Rating], [Secret], [SecretCreationTS]) VALUES (2016, N'Peter Parker', N'PETER PARKER', N'peterp@mail.com', N'PETERP@MAIL.COM', 1, N'AQAAAAEAACcQAAAAEFIKjZTSqqBcyn/jyMBXM/t1h8OksL83UcL7rORbU2Zjr2NBd/zMU+8jP7nxVM7DLw==', N'OLJNUUSQM2X4ZXSVQCFBFXQ4FBJMYILV', N'8a234805-8b78-46ec-bfcc-3b99eda43434', N'2514445555', 0, 0, NULL, 1, 0, N'123 My Street', N'Montreal', N'QC', N'H4H5H6', N'Peter_Parker.png', CAST(N'2020-06-08T12:42:46.1333333' AS DateTime2), 0, NULL, CAST(N'2020-06-08T12:42:46.1333333' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[AspNetUsers] OFF
GO
SET IDENTITY_INSERT [dbo].[Conversations] ON 
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1, 9, 1, N'Hello There!', 1, CAST(N'2020-06-05T00:21:27.983' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2, 1, 9, N'How you doing?', 1, CAST(N'2020-06-05T00:22:20.473' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1002, 9, 15, N'I am good', 1, CAST(N'2020-06-06T01:13:13.250' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1005, 9, 15, N'Is it still available?', 1, CAST(N'2020-06-06T01:25:23.773' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1006, 9, 15, N'Price is negotiable?', 1, CAST(N'2020-06-06T01:29:10.253' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1007, 9, 15, N'Hello', 1, CAST(N'2020-06-06T01:30:50.160' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (1008, 1015, 15, N'Hello everyone!', 1, CAST(N'2020-06-06T01:46:18.030' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2004, 9, 15, N'Hello', 1, CAST(N'2020-06-07T13:04:23.913' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2006, 9, 15, N'Are you there??', 1, CAST(N'2020-06-07T13:12:41.453' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2008, 8, 15, N'Hello', 1, CAST(N'2020-06-07T13:18:51.677' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2011, 8, 9, N'Hello Jerry', 1, CAST(N'2020-06-07T18:14:57.667' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2012, 9, 8, N'Hello Bob, how you doing?', 1, CAST(N'2020-06-07T18:15:08.140' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2013, 8, 9, N'is this still available?', 1, CAST(N'2020-06-07T18:15:30.340' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2014, 9, 8, N'yes', 1, CAST(N'2020-06-07T18:15:37.177' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2015, 8, 9, N'Is price negotiable?', 1, CAST(N'2020-06-07T18:15:48.623' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2080, 9, 8, N'Hi Bob!!!', 1, CAST(N'2020-06-08T12:45:10.530' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2081, 8, 9, N'Hello Jery!!', 1, CAST(N'2020-06-08T12:45:24.820' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2082, 1015, 9, N'Hi there!', 1, CAST(N'2020-06-08T15:50:36.530' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2083, 13, 9, N'Hello', 1, CAST(N'2020-06-08T15:54:57.573' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (2084, 9, 10, N'How are you?', 1, CAST(N'2020-06-08T15:55:29.127' AS DateTime))
GO
INSERT [dbo].[Conversations] ([Id], [SenderId], [ReceiverId], [Message], [Status], [CreationTS]) VALUES (3029, 9, 1, N'Hello', 1, CAST(N'2020-06-08T19:05:49.810' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Conversations] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (2, 1, N'1000 puzzle Sakura spring Japan', N'Just finished it. Gorgeous puzzle and fun Porch Pick up is from Dupont and Lansdowne area', CAST(18 AS Decimal(18, 0)), N'de992fbd-c615-498d-b8cd-ebf0632768f0.png', N'948f07ef-94ac-4715-a5ef-2be3b0f1e256.png', N'd3157f17-f595-408d-8999-3555b241cbc1.png', 1, CAST(N'2020-06-03T10:53:16.9100000' AS DateTime2), 6)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (3, 1, N'New Imaginarium Solar System Puzzle', N'New & unopened. 24 jumbo pieces. Learn all about solar system. 5+. Smoke & pet - free.', CAST(25 AS Decimal(18, 0)), N'69625ad3-ee63-436c-974b-ecdca3d63f14.jpg', N'c1481010-ad98-4584-ac4b-30965e452e51.png', N'10e68435-b4e2-482e-8813-3b64f64ab358.png', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 6)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (4, 1, N'Fish Jigsaw Puzzle', N'This is a very awesome puzzle and it has 1000 pieces. The puzzle looks very good', CAST(25 AS Decimal(18, 0)), N'1b9d5abf-38ec-4090-9d19-1a92288aa2f4.png', N'c3e777dd-a058-42c4-9af1-e61fb3c87be3.png', N'23643a5c-683c-4dde-adcc-6a7566253389.png', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 6)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (5, 8, N'PC games For children -various Elmo, My Little Pon', N'Various games for children. All in very good condition. Contact me for more options.', CAST(5 AS Decimal(18, 0)), N'e6aee84a-a885-4c07-9cdd-7d41bad32630.jpg', N'15111045-eb73-47b0-ab77-ab273e9ef47e.jpg', N'a3aa2e24-43a4-4188-ac87-626a5da1d6fd.jpg', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 6)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (6, 1, N'1960 Sunbeam Alpine', N'1960 Sunbeam Alpine Series 1 with non-synchronized 4 speed manual transmission. Restoration done in 2012 with numerous new parts and rebuilt Series 2 1592CC engine. 72250 original miles, driven only 1000 miles in last 7 summers, has never seen rain since restoration. New 2012 soft top on car. Unfinished hard top ( has all hardwares and wealther stripping) with customed made carrier included in price. Currently on classic Nardi sports steering wheel. Thousand spent on improvements. Car is in show condition ready to turns heads wherever she goes. Great summer cruise toy. Will provided further information upon serious inquiry. Asking $15000 as is, where is. Please don''t send any offer without seeing car, thanks.', CAST(15000 AS Decimal(18, 0)), N'62cabb76-88c6-4315-87ca-6afa5827a07f.jpg', N'a7ac4a43-4e58-43cb-ab8b-25f955d7fc1b.jpg', N'291bbc22-9a7d-4876-8928-816a3036ef98.jpg', 1, CAST(N'2020-06-03T10:56:10.7666667' AS DateTime2), 1)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (7, 8, N'Da Vinci Code', N'Da Vinci Code by Dan Brown. As new!', CAST(11 AS Decimal(18, 0)), N'80694b38-ed98-41a3-847f-8c6bf710099b.jpg', N'c0aa194a-eb8b-47e6-8840-090bd4d1eed7.jpg', N'8d9113cb-ef55-4c92-bfa1-625307c7882e.jpg', 1, CAST(N'2020-06-03T10:57:18.3633333' AS DateTime2), 4)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (8, 1, N'Double bed', N'Good condition! Mattress not included', CAST(100 AS Decimal(18, 0)), N'2f3dd791-4b7d-41c2-9561-24949a2ebab0.jpg', N'4e3cb588-f3f4-4df5-9a62-deb017265552.jpg', N'6ccbaad8-fdb4-4327-bca7-6a35712562cb.jpg', 1, CAST(N'2020-06-03T11:12:57.1700000' AS DateTime2), 5)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (9, 1, N'Costume - medieval style dress', N' 300/5000 Medieval style clothing. Blouse with lace at the collar, beige, in linen and cotton. Cotton, polyester and acrylic lace-up embroidered bodice. Pleated and long blue skirt in cotton and linen. Gray belt with suede finish. Wreath of dried burgundy flowers on hoop surrounded by ribbon.', CAST(50 AS Decimal(18, 0)), N'89f6c8b1-b48e-45a2-b970-679e5a652ec7.jpg', N'35002ea5-7c47-4f1e-9150-50e7a13e289b.jpg', N'5022e32b-fcfc-44b0-a698-78861efe62bc.jpg', 1, CAST(N'2020-06-03T12:03:23.2766667' AS DateTime2), 2)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (10, 9, N'17” Super Cycle Mountain Bike', N'18 speed SuperCycle Mountain bike for sale. Tune up required. Tires are flat, chain needs oiling. Great fixer upper project.', CAST(45 AS Decimal(18, 0)), N'5866ff11-8ee1-4548-8bd4-ee0b154bda94.jpg', N'72281719-8551-4212-8539-0ced9b3c512f.jpg', N'91c5212d-1b03-4096-8246-b08a46c8b751.jpg', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 7)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (11, 1, N'Yamaha YAS-203 sound bar + subwoofer', N'Yamaha YAS-203 sound bar + subwoofer. Wired or bluetooth connection, subwoofer connects over bluetooth so no wires necessary. Work perfectly, selling because moving to a smaller place and won''t need it there. Excellent sound quality, fidelity, and range.', CAST(200 AS Decimal(18, 0)), N'd915c6b1-e3e0-4696-a8ce-0330e8ebb940.jpg', N'838c56d7-4bf3-40a9-802a-8b8e0b2f8801.jpg', N'61fb3d77-f57c-4621-842f-7e31ad2a3cd4.jpg', 1, CAST(N'2020-06-03T12:16:01.4033333' AS DateTime2), 3)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (13, 7, N'Secret Book', N'Good condition', CAST(18 AS Decimal(18, 0)), N'6cd7ad99-4bd6-457e-b8ad-fc17dc016af1.jpg', N'6cd7ad99-4bd6-457e-b8ad-fc17dc016af1.jpg', N'6cd7ad99-4bd6-457e-b8ad-fc17dc016af1.jpg', 1, CAST(N'2020-06-04T11:19:06.2200000' AS DateTime2), 4)
GO
INSERT [dbo].[Products] ([Id], [UserId], [Title], [Description], [Price], [Image1], [Image2], [Image3], [Status], [PostedTS], [Category]) VALUES (15, 9, N'5 English books (scholastic) 11+', N'All these books are in great condition. They are all in English. They are for 11 year olds and up in my opinion and some are 13 and up. I can sell separately for 10$ but in that case you have to buy at least 2 books. Books: - Hatchet by Gary Paulson - Wings of fire by Tui T. Sutherland - Never say die by Will Hobbs - Take me to the river by Will Hobbs - The tiger rising by Kate DiCAMILLO', CAST(50 AS Decimal(18, 0)), N'c33d301d-17b8-467a-a103-d907333f5e65.jpg', N'8bd22961-7d02-4433-9716-21b489bb9bce.jpg', N'a3d73865-5f64-4a98-93ae-8fde73ac9159.jpg', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 4)
GO
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 2020-06-08 7:13:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT (getdate()) FOR [RegistrationTS]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT (getdate()) FOR [SecretCreationTS]
GO
ALTER TABLE [dbo].[Conversations] ADD  DEFAULT (getdate()) FOR [CreationTS]
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT (getdate()) FOR [PostedTS]
GO
ALTER TABLE [dbo].[Ratings] ADD  DEFAULT (getdate()) FOR [CreationTS]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Conversations]  WITH CHECK ADD  CONSTRAINT [FK_Conversations_ToTable] FOREIGN KEY([SenderId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Conversations] CHECK CONSTRAINT [FK_Conversations_ToTable]
GO
ALTER TABLE [dbo].[Conversations]  WITH CHECK ADD  CONSTRAINT [FK_Conversations_ToTable_1] FOREIGN KEY([ReceiverId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Conversations] CHECK CONSTRAINT [FK_Conversations_ToTable_1]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_ToTable] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_ToTable]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_RatedUserId] FOREIGN KEY([RatedUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_RatedUserId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_UserId]
GO
USE [master]
GO
ALTER DATABASE [kijijito_identity] SET  READ_WRITE 
GO
