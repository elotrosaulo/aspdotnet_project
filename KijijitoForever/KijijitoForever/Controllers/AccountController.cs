﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
//using AspNetCore;
using KijijitoForever.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;



namespace KijijitoForever.Controllers
{
    public class AccountController : Controller
    {   
        private readonly kijijito_identityContext _context;
        private UserManager<AppUser> UserMgr { get; }
        private SignInManager<AppUser> SignInMgr { get; }
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger<AccountController> logger;
        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, 
            IWebHostEnvironment webHostEnvironment, kijijito_identityContext context, ILogger<AccountController> logger) {

            UserMgr = userManager;
            SignInMgr = signInManager;
            _webHostEnvironment = webHostEnvironment;
            _context = context;
            this.logger = logger;

        }

        // /Account/Create
        // GET: Account/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,Password,PasswordRepeat,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount,Address,City,Province,PostalCode,PictureFile,Picture,RegistrationTs,Rating,Secret,SecretCreationTs")] AppUser appUser)
        {
            appUser.ExternalLogins = (await SignInMgr.GetExternalAuthenticationSchemesAsync()).ToList();

            if (ModelState.IsValid)
            {
                if (appUser.PictureFile != null)
                {
                    //in the product field from the Model Users put the name of the image file by combining the product id and the file extension
                    string targetPath = _webHostEnvironment.WebRootPath + "/userImages";
                    string filename = GenerateFileName(targetPath, appUser.PictureFile, appUser.UserName);
                    appUser.Picture = filename;
                    string path = Path.Combine(targetPath, filename);
                    //after that you save the file in the specific folder
                    using (var localFile = System.IO.File.OpenWrite(path))
                    using (var uploadedFile = appUser.PictureFile.OpenReadStream())
                    {
                        uploadedFile.CopyTo(localFile);
                    }
                }

                try
                {
                    ViewBag.Message = "User already registered";

                    AppUser user = await UserMgr.FindByNameAsync(appUser.UserName);
                    if (user == null)
                    {
                        IdentityResult result = await UserMgr.CreateAsync(appUser, appUser.Password);
                        ViewBag.Message = result;

                        if (result == IdentityResult.Success) {
                            var token = await UserMgr.GenerateEmailConfirmationTokenAsync(appUser); // Generate confirmation token
                            var confirmationLink = Url.Action("ConfirmEmail", "Account", new { userId = appUser.Id, token = token }, Request.Scheme);   // generate confirmation link
                            logger.Log(LogLevel.Warning, confirmationLink); // copy the link on the log
                            //Send emailwith SendInBlue API
                            // Configure API key authorization: api-key
                            Configuration.Default.ApiKey.TryAdd("api-key", "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");
                            Configuration.Default.ApiKey.TryAdd("partner-key","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");                                                  
                            // Email structure:         
                            var subject = "Kijijito - Activate your account";
                            var htmlContent = "Please click on the following link to activate your account :" + "<a href=" + confirmationLink + ">Activate your account</a>";
                            List<SendSmtpEmailTo> sendList = new List<SendSmtpEmailTo>();                           
                            var sendTo = new SendSmtpEmailTo(appUser.Email, appUser.UserName);
                            sendList.Add(sendTo);              
                            var sender = new SendSmtpEmailSender("No-Reply", "noreply@hotmail.com");
                            SendSmtpEmail sendSmtpEmail = new SendSmtpEmail(to: sendList, htmlContent: htmlContent, subject: subject, sender: sender);
                            SMTPApi stmpapi = new SMTPApi();
                            stmpapi.SendTransacEmail(sendSmtpEmail);
                            return RedirectToAction("CreationSuccessful");
                        }                      
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        // GET: Account/CreationSuccessful
        public IActionResult CreationSuccessful()
        {
            return View();
        }

        // /Account/Login
        // GET: Account/Login
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl)
        {
            AppUser model = new AppUser {
                ReturnUrl = returnUrl,
                ExternalLogins = (await SignInMgr.GetExternalAuthenticationSchemesAsync()).ToList()
            };
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(int? userId, string token) {
            if (userId == null || token == null) {
                return RedirectToAction("index", "home");
            }

            var user = UserMgr.FindByIdAsync(userId.ToString());
            AppUser userFound = await user;

            if (user == null) {
                ViewBag.ErrorMessage = $"The User ID {userId} is invalid";
                return View("NotFound");
            }

            var result = await UserMgr.ConfirmEmailAsync(userFound, token); // This convert EmailConfirmed from false to true

            if (result.Succeeded) {
                return View();
            }

            ViewBag.ErrorTitle = "Email cannot be confirmed";
            return View("Error");
        }

        // POST: Account/Login
        [HttpPost]
        public async Task<IActionResult> Login([Bind("UserName,Password,RememberMe")] AppUser appUser) {
            var result = await SignInMgr.PasswordSignInAsync(appUser.UserName, appUser.Password, appUser.RememberMe, false);
            appUser.ExternalLogins = (await SignInMgr.GetExternalAuthenticationSchemesAsync()).ToList();

            var user = await UserMgr.FindByNameAsync(appUser.UserName);

            // If user email is not confirmed yet (result = Not Authorized) and his pass doesnt not match with the username
            if (user != null && !user.EmailConfirmed && (await UserMgr.CheckPasswordAsync(user, appUser.Password))){
                ModelState.AddModelError(string.Empty, "Email not confirmed yet");
                return View(appUser);
            }

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
                //return RedirectToAction("Creation_Successful");
            }
            else
            {
                ViewBag.Result = "result is: " + result.ToString();
            }
            return View(appUser);
        }

        // GET: Account/Login
        [AllowAnonymous]
        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = SignInMgr.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        // GET: Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null) {
            returnUrl = returnUrl ?? Url.Content("~/");  // if it's null, we define it as "~/" (root url)

            AppUser model = new AppUser
            {
                ReturnUrl = returnUrl,
                ExternalLogins = (await SignInMgr.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            if (remoteError != null) {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");

                return View("Login", model);
            }

            var info = await SignInMgr.GetExternalLoginInfoAsync();
            if (info == null) { // so we didnt receive any information from the login provider
                ModelState.AddModelError(string.Empty, "Error loading external login information.");
                return View("Login", model);
            }

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            AppUser user = null;

            if (email != null)
            {
                user = await UserMgr.FindByEmailAsync(email);
                if (user != null && !user.EmailConfirmed) {
                    ModelState.AddModelError(string.Empty, "Email not confirmed yet");
                    return View("Login", model);
                }
            }

            var signInResult = await SignInMgr.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, 
                                                                        isPersistent: false, bypassTwoFactor: true);
            if (signInResult.Succeeded)

            {   // if it found the id registered as user in the db and succeded
                return LocalRedirect(returnUrl);
            }
            else {
                if (email != null) {
                    if (user == null) {
                        if (info.LoginProvider == "Facebook") {
                            user = new AppUser
                            {
                                UserName = info.Principal.FindFirstValue(ClaimTypes.Name),
                                Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                            };
                        }
                        else
                        {
                            user = new AppUser
                            {
                                UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                                Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                            };
                        }
 
                        await UserMgr.CreateAsync(user);
                        var token = await UserMgr.GenerateEmailConfirmationTokenAsync(user); // Generate confirmation token
                        var confirmationLink = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = token }, Request.Scheme);   // generate confirmation link
                        logger.Log(LogLevel.Warning, confirmationLink); // copy the link on the log
                        //Send emailwith SendInBlue API
                        // Configure API key authorization: api-key
                        Configuration.Default.ApiKey.TryAdd("api-key", "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");
                        Configuration.Default.ApiKey.TryAdd("partner-key","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");                                                  
                        // Email structure:         
                        var subject = "Kijijito - Activate your account";
                        var htmlContent = "Please click on the following link to activate your account :" + "<a href=" + confirmationLink + ">Activate your account</a>";
                        List<SendSmtpEmailTo> sendList = new List<SendSmtpEmailTo>();                           
                        var sendTo = new SendSmtpEmailTo(user.Email, user.UserName);
                        sendList.Add(sendTo);              
                        var sender = new SendSmtpEmailSender("No-Reply", "noreply@hotmail.com");
                        SendSmtpEmail sendSmtpEmail = new SendSmtpEmail(to: sendList, htmlContent: htmlContent, subject: subject, sender: sender);
                        SMTPApi stmpapi = new SMTPApi();
                        stmpapi.SendTransacEmail(sendSmtpEmail);
                        return RedirectToAction("CreationSuccessful");
                    }

                    await UserMgr.AddLoginAsync(user, info);    // to add a row in the AspNetUserLogins table
                    await SignInMgr.SignInAsync(user, isPersistent: false); // to sign in the user

                    return LocalRedirect(returnUrl);
                }

                ViewBag.ErrorTitle = $"Email claim not received from: {info.LoginProvider}";
                ViewBag.ErrorMessage = $"Please contact support from Kijijito creators";

                return View("Error");
            }
        }

        // /Account/Logout
        public async Task<IActionResult> Logout() {
            await SignInMgr.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        // /Account/ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword() {
            return View();
        }

        // /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(AppUser appUser)
        {
            if (appUser.Email != null) {
                var user = await UserMgr.FindByEmailAsync(appUser.Email);
                if (user != null && await UserMgr.IsEmailConfirmedAsync(user)) {
                    var token = await UserMgr.GeneratePasswordResetTokenAsync(user);
                    var passwordResetLink = Url.Action("ResetPassword", "Account", new { email = appUser.Email, token = token }, Request.Scheme);
                    logger.Log(LogLevel.Warning, passwordResetLink);
                    //Send emailwith SendInBlue API
                    // Configure API key authorization: api-key
                    Configuration.Default.ApiKey.TryAdd("api-key", "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");
                    Configuration.Default.ApiKey.TryAdd("partner-key", "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");
                    // Email structure:         
                    var subject = "Kijijito - Reset password";
                    var htmlContent = "Please click on the following link to reset your password :" + "<a href=" + passwordResetLink + ">Click here to reset</a>";
                    List<SendSmtpEmailTo> sendList = new List<SendSmtpEmailTo>();
                    var sendTo = new SendSmtpEmailTo(user.Email, user.UserName);
                    sendList.Add(sendTo);
                    var sender = new SendSmtpEmailSender("No-Reply", "noreply@hotmail.com");
                    SendSmtpEmail sendSmtpEmail = new SendSmtpEmail(to: sendList, htmlContent: htmlContent, subject: subject, sender: sender);
                    SMTPApi stmpapi = new SMTPApi();
                    stmpapi.SendTransacEmail(sendSmtpEmail);

                    return View("ForgotPasswordConfirmation");
                }
            }
            return View(appUser);
        }


        // /Account/ResetPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            if (token == null || email == null) {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }


        // /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(AppUser appUser)
        {
            if (appUser.Email != null && appUser.Token != null)
            {
                var user = await UserMgr.FindByEmailAsync(appUser.Email);
                if (user != null)
                {
                    var result = await UserMgr.ResetPasswordAsync(user, appUser.Token, appUser.Password);
                    if (result.Succeeded) {
                        return View("ResetPasswordConfirmation");
                    }
                    foreach (var error in result.Errors) {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(appUser);
                }
                return View("ResetPasswordConfirmation");
            }
            return View(appUser);
        }


        // GET: Account/Details/name
        public async Task<IActionResult> Details(
            #nullable enable
            string? UserName)
        {
            if (UserName == null)
            {
                return NotFound();
            }

            var appUsers = await UserMgr.FindByNameAsync(UserName);
            if (appUsers == null)
            {
                return NotFound();
            }

            return View(appUsers);
        }

        // GET: AspNetUsers/Edit/username
        public async Task<IActionResult> Edit(
            #nullable enable 
            string? UserName)
        {
            if (UserName == null)
            {
                return NotFound();
            }

            var appUsers = await UserMgr.FindByNameAsync(UserName);
            if (appUsers == null)
            {
                return NotFound();
            }
            return View(appUsers);
        }

        // POST: AspNetUsers/Edit/username
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string UserName, [Bind("Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,Password,PasswordRepeat,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount,Address,City,Province,PostalCode,PictureFile,Picture,RegistrationTs,Rating,Secret,SecretCreationTs")] AppUser appUser)
        {
            if (UserName != appUser.UserName)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // To get current user's id:
                    //var user = await UserMgr.GetUserAsync(HttpContext.User);
                    //var userId = user.Id;

                    var appUsersEdit = await UserMgr.FindByIdAsync(appUser.Id.ToString());
                    appUsersEdit.UserName = appUser.UserName;
                    appUsersEdit.PhoneNumber = appUser.PhoneNumber;
                    appUsersEdit.Address = appUser.Address;
                    appUsersEdit.City = appUser.City;
                    appUsersEdit.Province = appUser.Province;
                    appUsersEdit.PostalCode = appUser.PostalCode;
                    appUsersEdit.PhoneNumber = appUser.PhoneNumber;

                    if (appUser.PictureFile != null)
                    {
                        //in the product field from the Model Users put the name of the image file by combining the product id and the file extension
                        string targetPath = _webHostEnvironment.WebRootPath + "/userImages";
                        string filename = GenerateFileName(targetPath, appUser.PictureFile, appUser.UserName);
                        appUsersEdit.Picture = filename;
                        string path = Path.Combine(targetPath, filename);
                        //after that you save the file in the specific folder
                        using (var localFile = System.IO.File.OpenWrite(path))
                        using (var uploadedFile = appUser.PictureFile.OpenReadStream())
                        {
                            uploadedFile.CopyTo(localFile);
                        }
                    }
                    else
                    {
                        appUsersEdit.Picture = appUser.Picture;
                    }

                    var result = await UserMgr.UpdateAsync(appUsersEdit);
                    //ViewBag.Message = result;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppUsersExists(appUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { UserName = appUser.UserName });
            }
            return View(appUser);
        }

        // GET: AspNetUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appUser = await UserMgr.FindByIdAsync(id.ToString());
            if (appUser == null)
            {
                return NotFound();
            }

            return View(appUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appUser = await UserMgr.FindByIdAsync(id.ToString());
            var result = await UserMgr.DeleteAsync(appUser);
             
            return RedirectToAction(nameof(id));
        }

        private bool AppUsersExists(int id)
        {
            var result = UserMgr.FindByIdAsync(id.ToString());
            if (result != null)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public string GenerateFileName(string TargetPath, IFormFile file, string name)
        {
            string ReturnValue;
            string extension = Path.GetExtension(file.FileName);
            string FileName = name.Replace(' ', '_');
            ReturnValue = FileName + extension;
            if (!System.IO.File.Exists(Path.Combine(TargetPath, ReturnValue)))
            {
                return ReturnValue;
            }
            // This part creates a recursive pattern to ensure that you will not overwrite an existing file
            return GenerateFileName(TargetPath, file, name);
        }

    }
}
