﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace KijijitoForever.Models
{
    public class ProductsController : Controller
    {
        private readonly kijijito_identityContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly UserService _userService;

        public ProductsController(kijijito_identityContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _userService = new UserService(_context);
        }

        // GET: Products
        public async Task<IActionResult> Index(ProductCategoryEnum? Category = null, string SearchString = "", string CurrentFilter = "", int? UserId = null, int? pageNumber = null)
        {
            ViewData["CurrentCategory"] = Category;
            if (!String.IsNullOrEmpty(SearchString))
            {
                pageNumber = 1;
            }
            else
            {
                SearchString = CurrentFilter;
            }
            ViewData["CurrentFilter"] = SearchString;
            var products = from p in _context.Products.Where(p => p.Status != ProductStatusEnum.Deleted).OrderByDescending(p => p.Id).Include(p => p.User) select p;
            if (Category != null)
            {
                products = products.Where(p => p.Category == Category);
            }
            ViewData["CurrentUserId"] = UserId;
            if (UserId != null) {
                products = products.Where(p => p.UserId == UserId);
            }
            if (!String.IsNullOrEmpty(SearchString))
            {
                products = products.Where(s => s.Title.ToUpper().Contains(SearchString.ToUpper())
                                       || s.Description.ToUpper().Contains(SearchString.ToUpper()));
            }
            int pageSize = 3;
            return View(await PaginatedList<Products>.CreateAsync(products.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (User.Identity.Name != null)
            {
                var currUserId = await _userService.GetCurrentUserId(User.Identity.Name);
                ViewBag.CurrentId = currUserId;
            }
            else
            {
                ViewBag.CurrentId = 0;
            }
            var products = await _context.Products
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (products == null)
            {
                return NotFound();
            }

            return View(products);
        }

        // GET: Products/Create
        [Authorize]
        public IActionResult Create()
        {
            //ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Id");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,UserId,Title,Description,Price,ImageFile1,ImageFile2,ImageFile3,Status,PostedTs,Category")] Products products)
        { 
            products.UserId = await _userService.GetCurrentUserId(User.Identity.Name);
            if (ModelState.IsValid)
            {
                string targetPath = _webHostEnvironment.WebRootPath + "/productImages";
                if (products.ImageFile1 != null)
                {
                    // in the product field from the Model Product put the name of the image file by combining the product id and the file extension   
                    string filename1 = GenerateFileName(targetPath, products.ImageFile1);
                    // assign new file name to Image1 field to save on database
                    products.Image1 = filename1;
                    // move image to folder
                    SaveImageToFolder(targetPath, filename1, products.ImageFile1);
                }
                if (products.ImageFile2 != null)
                {
                    string filename2 = GenerateFileName(targetPath, products.ImageFile2);
                    products.Image2 = filename2;
                    SaveImageToFolder(targetPath, filename2, products.ImageFile2);
                }
                else
                {
                    products.Image2 = products.Image1;
                }
                if (products.ImageFile3 != null)
                {
                    string filename3 = GenerateFileName(targetPath, products.ImageFile3);
                    products.Image3 = filename3;
                    SaveImageToFolder(targetPath, filename3, products.ImageFile3);
                }
                else
                {
                    products.Image3 = products.Image1;
                }

                _context.Add(products);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
           // ViewData["UserId"] = new SelectList(_context.AspNetUsers, "Id", "Email", products.UserId);
            return View(products);
        }

        // GET: Products/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var products = await _context.Products.FindAsync(id);
            if (products == null)
            {
                return NotFound();
            }
            return View(products);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,Title,Description,Price,ImageFile1,ImageFile2,ImageFile3,Image1,Image2,Image3,Status,Category")] Products products) //[Bind("Id,UserId,Title,Description,Price,Image1,Image2,Image3,Status,PostedTs,Category")]
        { 
            if (id != products.Id)
            {
                return NotFound();
            }
            products.UserId = await _userService.GetCurrentUserId(User.Identity.Name);
            if (ModelState.IsValid)
            {
                try
                {
                string targetPath = _webHostEnvironment.WebRootPath + "/productImages";
                if (products.ImageFile1 != null)
                {
                    //in the product field from the Model Product put the name of the image file by combining the product id and the file extension   
                    string filename1 = GenerateFileName(targetPath, products.ImageFile1);
                    products.Image1 = filename1;
                    SaveImageToFolder(targetPath, filename1, products.ImageFile1);
                }
                else
                    {
                        products.Image1 = products.Image1;
                    }
                if (products.ImageFile2 != null)
                {
                    string filename2 = GenerateFileName(targetPath, products.ImageFile2);
                    products.Image2 = filename2;
                    SaveImageToFolder(targetPath, filename2, products.ImageFile2);
                }
                    else
                    {
                        products.Image2 = products.Image2;
                    }
                    if (products.ImageFile3 != null)
                {
                    string filename3 = GenerateFileName(targetPath, products.ImageFile3);
                    products.Image3 = filename3;
                    SaveImageToFolder(targetPath, filename3, products.ImageFile3);
                }
                    else
                    {
                        products.Image3 = products.Image3;
                    }


                    _context.Update(products);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductsExists(products.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(products);
        }

        // GET: Products/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var products = await _context.Products
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (products == null)
            {
                return NotFound();
            }

            return View(products);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var products = await _context.Products.FindAsync(id);
            products.Status = ProductStatusEnum.Deleted;
            try
            {
                _context.Update(products);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(products.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
            //_context.Products.Remove(products);
            //await _context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
        }

        private bool ProductsExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }

        public string GenerateFileName(string targetPath, IFormFile file)
        {
            string ReturnValue;
            string extension = Path.GetExtension(file.FileName);
            string FileName = Guid.NewGuid().ToString();
            ReturnValue = FileName + extension;
            if (!System.IO.File.Exists(Path.Combine(targetPath, ReturnValue)))
            {
                return ReturnValue;
            }
            // This part creates a recursive pattern to ensure that you will not overwrite an existing file
            return GenerateFileName(targetPath, file);
        }

        public void SaveImageToFolder(string targetPath, string filename, IFormFile file)
        {
            string path = Path.Combine(targetPath, filename);
            //after that you save the file in the specific folder
            using (var localFile = System.IO.File.OpenWrite(path))
            using (var uploadedFile = file.OpenReadStream()) // products.ImageFile1
            {
                uploadedFile.CopyTo(localFile);
            }
        }
    }
}
