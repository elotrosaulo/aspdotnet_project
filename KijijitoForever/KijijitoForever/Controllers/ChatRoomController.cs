﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KijijitoForever.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KijijitoForever.Views.ChatRoom
{
    public class ChatRoomController : Controller
    {
        private readonly kijijito_identityContext _context;
        public readonly UserManager<AppUser> _userManager;
        private readonly UserService _userService;

        public ChatRoomController(kijijito_identityContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _userService = new UserService(_context);
        }

        [Authorize]
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var receiverUser = await _context.AspNetUsers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (receiverUser == null)
            {
                return NotFound();
            }
            ViewBag.ReceiverId = id;
            ViewBag.UserReceiver = receiverUser;
            var currUserName = User.Identity.Name;
            var currUserId = await _userService.GetCurrentUserId(User.Identity.Name);
            ViewBag.CurrentUserName = currUserName;
            ViewBag.CurrentId = currUserId;

            var messagesQuery = _context.Conversations.Where(p => p.SenderId == currUserId || p.ReceiverId == currUserId).Where(p => p.SenderId == id || p.ReceiverId == id).OrderBy(p => p.CreationTs).Include(p => p.Sender).Include(p => p.Receiver).ToList();

            ConversationVM myModel = new ConversationVM();
            //myModel.Messages = _context.Conversations.OrderBy(p => p.CreationTs).Include(p => p.Sender).Include(p => p.Receiver).ToList(); // FIX ME: Filter by users
            myModel.Messages = messagesQuery;
            myModel.NewMessage = new Conversations();
            return View(myModel);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public void Create(MessageForm conversation)
        {
            //Console.WriteLine("Trying to add a new conversation to database");
            Conversations conversationToSave = new Conversations();
            conversationToSave.SenderId = conversation.SenderId;
            conversationToSave.ReceiverId = conversation.ReceiverId;
            conversationToSave.Message = conversation.Message;
            conversationToSave.Status = ConversationStatusEnum.Sent;
            _context.Add(conversationToSave);
            _context.SaveChanges();
        }
    }
}