﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace KijijitoForever.Models
{
    public class AspNetUsersController : Controller
    {
        private readonly kijijito_identityContext _context;
        //private UserManager<AspNetUsers> UserMgr { get; }
        //private SignInManager<AspNetUsers> SignInMgr { get; }
        
        //private readonly IWebHostEnvironment _webHostEnvironment;

        public AspNetUsersController(kijijito_identityContext context)//, UserManager<AspNetUsers> userManager, SignInManager<AspNetUsers> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            //UserMgr = userManager;
            //SignInMgr = signInManager;
          //  _webHostEnvironment = webHostEnvironment;
        }

        // GET: AspNetUsers
        public async Task<IActionResult> Index()
        {
            return View(await _context.AspNetUsers.ToListAsync());
        }

        // GET: AspNetUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aspNetUsers = await _context.AspNetUsers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aspNetUsers == null)
            {
                return NotFound();
            }

            return View(aspNetUsers);
        }

        // GET: AspNetUsers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,Password,PasswordRepeat,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount,Address,City,Province,PostalCode,PictureFile,Picture,RegistrationTs,Rating,Secret,SecretCreationTs")] AspNetUsers aspNetUsers)
        {
            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        ViewBag.Message = "User already registered";

            //        AspNetUsers user = await UserMgr.FindByNameAsync(aspNetUsers.UserName);
            //        if (user == null)
            //        {
            //            IdentityResult result = await UserMgr.CreateAsync(aspNetUsers, aspNetUsers.Password);
            //            ViewBag.Message = "User was created";
            //            return RedirectToAction(nameof(Index));
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        ViewBag.Message = ex.Message;
            //    }
                //return View();

            if (ModelState.IsValid)
                {
                    _context.Add(aspNetUsers);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            return View();
        }

        // GET: AspNetUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aspNetUsers = await _context.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return NotFound();
            }
            return View(aspNetUsers);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount,Address,City,Province,PostalCode,Picture,RegistrationTs,Rating,Secret,SecretCreationTs")] AspNetUsers aspNetUsers)
        {
            if (id != aspNetUsers.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aspNetUsers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AspNetUsersExists(aspNetUsers.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aspNetUsers);
        }

        // GET: AspNetUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aspNetUsers = await _context.AspNetUsers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aspNetUsers == null)
            {
                return NotFound();
            }

            return View(aspNetUsers);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aspNetUsers = await _context.AspNetUsers.FindAsync(id);
            _context.AspNetUsers.Remove(aspNetUsers);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AspNetUsersExists(int id)
        {
            return _context.AspNetUsers.Any(e => e.Id == id);
        }
    }
}
