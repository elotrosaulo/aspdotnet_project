﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KijijitoForever.Models;
using System.Dynamic;
using Microsoft.AspNetCore.Authorization;

namespace KijijitoForever.Controllers
{
    public class ConversationsController : Controller
    {
        private readonly kijijito_identityContext _context;
        private readonly UserService _userService;

        public ConversationsController(kijijito_identityContext context)
        {
            _context = context;
            _userService = new UserService(_context);
        }

        // GET: Conversations
        [Authorize]
        public async Task<IActionResult> Index(int? userid)
        {
            var query1 = _context.Conversations.Where(p => p.ReceiverId == userid).Select(p => p.SenderId).ToList();
            var query2 = _context.Conversations.Where(p => p.SenderId == userid).Select(p => p.ReceiverId).ToList();
            var ids = query1.Union(query2).ToList();

            var users = new List<AspNetUsers>();
            foreach (var id in ids)
            {
                var user = await _context.AspNetUsers.FirstOrDefaultAsync(m => m.Id == id);
                users.Add(user);
            }
            return View(users);
        }

        // GET: Conversations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conversations = await _context.Conversations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (conversations == null)
            {
                return NotFound();
            }

            return View(conversations);
        }

        // GET: Conversations/Create
        //public IActionResult Create()
        //{
        //    return View();
        //}

        public async Task<IActionResult> Create() // ([Bind(Prefix = "NewMessage")] Conversations _conversation) // createAsync?
        {
            ViewBag.CurrentUserName = User.Identity.Name;
            ViewBag.CurrentId = _userService.GetCurrentUserId(User.Identity.Name);
            return View();
        }

        // POST: Conversations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SenderId,ReceiverId,Message,Status,CreationTs")] Conversations conversations)
        {
            conversations.SenderId = await _userService.GetCurrentUserId(User.Identity.Name);
            conversations.ReceiverId = 1;
            if (ModelState.IsValid)
            {
                _context.Add(conversations);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create));
            }
            return RedirectToAction(nameof(Create));
        }

        // GET: Conversations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conversations = await _context.Conversations.FindAsync(id);
            if (conversations == null)
            {
                return NotFound();
            }
            return View(conversations);
        }

        // POST: Conversations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SenderId,ReceiverId,Message,Status,CreationTs")] Conversations conversations)
        {
            if (id != conversations.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(conversations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConversationsExists(conversations.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(conversations);
        }

        // GET: Conversations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conversations = await _context.Conversations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (conversations == null)
            {
                return NotFound();
            }

            return View(conversations);
        }

        // POST: Conversations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var conversations = await _context.Conversations.FindAsync(id);
            _context.Conversations.Remove(conversations);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ConversationsExists(int id)
        {
            return _context.Conversations.Any(e => e.Id == id);
        }
    }
}
