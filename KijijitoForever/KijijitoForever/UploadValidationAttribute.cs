﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class UploadValidationAttribute : ValidationAttribute
    {
        //private string FileIsNull = "No file uploaded!";
        private string FileTooBig = "Uploaded file is too big (maximum size is " + MaximumFileSize + " B)";
        private string FileIsNotImg = "Invalid image format. It should be .jpg, .jpeg, .png, or .gif";
        private const int MaximumFileSize = 5 * 1024 * 1024; // equals to 5 MB

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IFormFile file = value as IFormFile;
            if (file == null)
            {
                return ValidationResult.Success;
            }

            if (file.Length > MaximumFileSize)
            {
                return new ValidationResult(FileTooBig);
            }

            string ext = Path.GetExtension(file.FileName);
            var isimg = false;
            switch (ext)
            {
                case ".jpg": isimg = true; break;
                case ".png": isimg = true; break;
                case ".jpeg": isimg = true; break;
                case ".gif": isimg = true; break;
            }
            if (!isimg)
            {
                return new ValidationResult(FileIsNotImg);
            }
            return ValidationResult.Success;
        }
    }
}
