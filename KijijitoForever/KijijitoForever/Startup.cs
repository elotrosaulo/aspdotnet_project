using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KijijitoForever.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;
using System.Text.Json.Serialization;
using KijijitoForever.Hubs;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;

namespace KijijitoForever
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IdentityAppContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<kijijito_identityContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<AppUser, AppRole>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ ";
                options.SignIn.RequireConfirmedEmail = true;
            }).AddEntityFrameworkStores < IdentityAppContext>()
            .AddDefaultTokenProviders();

            services.Configure<DataProtectionTokenProviderOptions>(o => 
                                o.TokenLifespan = TimeSpan.FromHours(12));       // Password reset and confirmation token life is set to 12 hours. By default is 24 hours

            services.AddControllersWithViews();
            //
            services.AddRazorPages();
            services
                .AddMvc()
                // Or .AddControllers(...)
                .AddJsonOptions(opts =>
                {
                    opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.AddAuthentication().AddGoogle(options =>
            {
                options.ClientId = "201951229663-aijjb3gjshod9du6frvfci8d60nmf9sk.apps.googleusercontent.com";
                options.ClientSecret = "MEWrikxYT4rZc8ANhWLD86m2";
            }).AddFacebook(options => {
                options.AppId = "254223379194759";
                options.AppSecret = "19d8182cf2c1100f8c9a14e875f680c8";
            });

            services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [Obsolete]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<ChatHub>("/Chat/Index");
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");    
                endpoints.MapRazorPages(); //
                endpoints.MapHub<ChatHub>("/chatHub");

            });
        }
    }
}
