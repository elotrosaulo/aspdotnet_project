﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KijijitoForever.Models
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            AspNetUserTokens = new HashSet<AspNetUserTokens>();
            ConversationsReceiver = new HashSet<Conversations>();
            ConversationsSender = new HashSet<Conversations>();
            Products = new HashSet<Products>();
            RatingsRatedUser = new HashSet<Ratings>();
            RatingsUser = new HashSet<Ratings>();
        }

        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z''-' \s]{1,50}$", ErrorMessage = "Name lenght should be 1 - 50 characters long and only contain letters, spaces and '-' characters.")]
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid email format.")]
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }

        [NotMapped]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{5,20}$", ErrorMessage = "Password should have at least 1 uppercase, 1 lowercase and 1 number character. It should be 1 - 20 characters long.")]
        public string Password { get; set; }

        [NotMapped]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{5,20}$", ErrorMessage = "Password should have at least 1 uppercase, 1 lowercase and 1 number character. It should be 1 - 20 characters long.")]
        [Compare("Password", ErrorMessage = "Both password fields must be identical.")]
        public string PasswordRepeat { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        
        [StringLength(20, ErrorMessage = "Phone number should not be more than 20 characters long.", MinimumLength = 1)]
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9''-'#\. \s]{1,100}$", ErrorMessage = "Address lenght should be 1 - 100 characters long and only contain letters, spaces and '#-' characters.")]
        public string Address { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9''-' \s]{1,50}$", ErrorMessage = "City lenght should be 1 - 50 characters long and only contain letters, spaces and '-' characters.")]
        public string City { get; set; }

        [RegularExpression(@"^(?:AB|BC|MB|N[BLTSU]|ON|PE|QC|SK|YT)*$", ErrorMessage = "Invalid province abreviation.")]
        public string Province { get; set; }

        [RegularExpression(@"^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$", ErrorMessage = "Invalid Postal Code format.")]
        public string PostalCode { get; set; }

        [NotMapped]
        [DisplayName("Upload File")]
        [UploadValidationAttribute]
        public IFormFile PictureFile { get; set; }
        public string Picture { get; set; }

        [Timestamp]
        public DateTime RegistrationTs { get; set; }

        [Range(0.00, 5.00, ErrorMessage = "Rating should be a valid number between 0 and 5")]
        public float Rating { get; set; }

        [StringLength(100)]
        public string Secret { get; set; }

        [Timestamp]
        public DateTime SecretCreationTs { get; set; }

        [NotMapped]
        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        public virtual ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual ICollection<AspNetUserTokens> AspNetUserTokens { get; set; }

        public virtual ICollection<Products> Products { get; set; }
        public virtual ICollection<Conversations> ConversationsReceiver { get; set; }
        public virtual ICollection<Conversations> ConversationsSender { get; set; }
        public virtual ICollection<Ratings> RatingsRatedUser { get; set; }
        public virtual ICollection<Ratings> RatingsUser { get; set; }

    }
}
