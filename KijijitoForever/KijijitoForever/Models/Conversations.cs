﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KijijitoForever.Models
{
    public partial class Conversations
    {
        
        public int Id { get; set; }
        [Required]
        public int SenderId { get; set; }
        [Required]
        public int ReceiverId { get; set; }
        [Required]
        public string Message { get; set; }
        public ConversationStatusEnum Status { get; set; }
        public DateTime CreationTs { get; set; }

        public virtual AspNetUsers Receiver { get; set; }
        public virtual AspNetUsers Sender { get; set; }
    }

    public enum ConversationStatusEnum : int
    {
        Sent = 1,
        Received = 2
    }
}
