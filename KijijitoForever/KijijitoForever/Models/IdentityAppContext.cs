﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever.Models
{
    public class IdentityAppContext : IdentityDbContext<AppUser, AppRole, int>  // User class = AppUSer, Role class = AppRole, primary key for our class = int
    {
        public IdentityAppContext(DbContextOptions<IdentityAppContext> options) : base(options) { 
            //UserValidator = new UserValidator<AppUser>(this) { AllowOnlyAlphanumericUserNames = false }
        }
    }
}
