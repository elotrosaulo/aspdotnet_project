﻿using System;
using System.Collections.Generic;

namespace KijijitoForever.Models
{
    public partial class Ratings
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RatedUserId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public DateTime CreationTs { get; set; }

        public virtual AspNetUsers RatedUser { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
