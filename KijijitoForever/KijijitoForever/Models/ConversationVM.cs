﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever.Models
{
    public class ConversationVM
    {
        public IEnumerable<Conversations> Messages { get; set; }
        public Conversations NewMessage { get; set; }
    }
}
