﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever.Models
{
    public class AppUser : IdentityUser<int>
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9''-'@. \s]{1,50}$", ErrorMessage = "Name lenght should be 1 - 50 characters long and only contain letters, spaces, numbers and '@', '-' characters.")]
        public override string UserName { get; set; }
        public override string NormalizedUserName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid email format.")]
        public override string Email { get; set; }
        //public override string NormalizedEmail { get; set; }
        public override bool EmailConfirmed { get; set; }

       // [Required]
        [NotMapped]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[a-zA-Z\d@$!%*?&]{5,20}$", ErrorMessage = "Password should have at least 1 uppercase, 1 lowercase, 1 number and 1 special character. It should be 1 - 20 characters long.")]
        public string Password { get; set; }

        // [Required]
        [NotMapped]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[a-zA-Z\d@$!%*?&]{5,20}$", ErrorMessage = "Password should have at least 1 uppercase, 1 lowercase, 1 number and 1 special character. It should be 1 - 20 characters long.")]
        [Compare("Password", ErrorMessage = "Both password fields must be identical.")]
        public string PasswordRepeat { get; set; }
        public override string PasswordHash { get; set; }
        //public override string SecurityStamp { get; set; }
        //public override string ConcurrencyStamp { get; set; }

        [StringLength(20, ErrorMessage = "Phone number should not be more than 20 characters long.", MinimumLength = 1)]
        public override string PhoneNumber { get; set; }
        public override bool PhoneNumberConfirmed { get; set; }
        public override bool TwoFactorEnabled { get; set; }
        public override DateTimeOffset? LockoutEnd { get; set; }
        public override bool LockoutEnabled { get; set; }
        public override int AccessFailedCount { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9''-'#\. \s]{1,100}$", ErrorMessage = "Address lenght should be 1 - 100 characters long and only contain letters, spaces and '#-' characters.")]
        public string Address { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9''-' \s]{1,50}$", ErrorMessage = "City lenght should be 1 - 50 characters long and only contain letters, spaces and '-' characters.")]
        public string City { get; set; }

        [RegularExpression(@"^(?:AB|BC|MB|N[BLTSU]|ON|PE|QC|SK|YT)*$", ErrorMessage = "Invalid province abreviation.")]
        public string Province { get; set; }

        [RegularExpression(@"^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$", ErrorMessage = "Invalid Postal Code format.")]
        public string PostalCode { get; set; }

        [NotMapped]
        [DisplayName("Upload File")]
        [UploadValidationAttribute]
        public IFormFile PictureFile { get; set; }
        public string Picture { get; set; }

        [Timestamp]
        public DateTime RegistrationTS { get; set; }

        [Range(0.00, 5.00, ErrorMessage = "Rating should be a valid number between 0 and 5")]
        public float Rating { get; set; }

        [StringLength(100)]
        public string Secret { get; set; }

        [Timestamp]
        public DateTime? SecretCreationTS { get; set; }

        [NotMapped]
        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        [NotMapped]
        public string ReturnUrl { get; set; }   // Url user is trying to access before authentication, so we can return after authentication

        [NotMapped]
        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        [NotMapped]
        public String Token { get; set; }






        //public string Address { get; set; }
        //public string City { get; set; }
        //public string Province { get; set; }
        //public string PostalCode { get; set; }
        //public string Picture { get; set; }
        //public DateTime RegistrationTS { get; set; }
        //public float Rating { get; set; }
        //public string Secret { get; set; }
        //public DateTime SecretCreationTS { get; set; }
    }
}
