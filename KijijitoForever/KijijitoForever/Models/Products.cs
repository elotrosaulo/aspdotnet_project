﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KijijitoForever.Models
{
    public partial class Products
    {
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "{0} should be between 5 to 50 characters")]
        public string Title { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 10, ErrorMessage = "{0} should be between 10 to 1000 characters")]
        public string Description { get; set; }

        [Range(0, 49999.99)]
        public decimal Price { get; set; }

        [NotMapped]
        [DisplayName("Upload Picture 1")]
        [UploadValidationAttribute]
        public IFormFile ImageFile1 { get; set; }

        [NotMapped]
        [DisplayName("Upload Picture 2")]
        [UploadValidationAttribute]
        public IFormFile ImageFile2 { get; set; }

        [NotMapped]
        [DisplayName("Upload Picture 3")]
        [UploadValidationAttribute]
        public IFormFile ImageFile3 { get; set; }

        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public ProductStatusEnum Status { get; set; }
        public DateTime PostedTs { get; set; }
        public ProductCategoryEnum? Category { get; set; }

        public virtual AspNetUsers User { get; set; }
    }

    public enum ProductStatusEnum : int
    {
        Available = 1,
        Sold = 2,
        Deleted = 3
    }

    public enum ProductCategoryEnum : int
    {
        Cars = 1,
        Clothing = 2,
        Electronics = 3,
        Entertainment = 4,
        [Display(Name = "Home & Appliances")]
        HomeAppliances = 5,
        [Display(Name = "Toys & Games")]
        ToysAndGames = 6,
        Other = 7
    }



}
