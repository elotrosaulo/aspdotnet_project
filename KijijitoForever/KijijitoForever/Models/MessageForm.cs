﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever.Models
{
    public class MessageForm
    {
      
      
        public int SenderId { get; set; }
      
        public int ReceiverId { get; set; }
       
        public string Message { get; set; }
        public string Status { get; set; }
       
    }
}
