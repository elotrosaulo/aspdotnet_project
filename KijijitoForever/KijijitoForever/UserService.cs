﻿using KijijitoForever.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KijijitoForever
{
    public class UserService
    {
        private readonly kijijito_identityContext _context;

        public UserService (kijijito_identityContext context)
        {
            _context = context;
        }

        public async Task<int> GetCurrentUserId(string username)
        {
            AspNetUsers currentUser = await _context.AspNetUsers.FirstOrDefaultAsync(m => m.UserName == username);
            return currentUser.Id;
        }

    }
}
